'use strict'

class Student {
  constructor(name, email, grades) {
    this._name = name
    this._email = email
    this._avg = undefined
    this._passed = undefined
    this._withHonors = undefined

    if (Array.isArray(grades)) {
      if (grades.length === 4) {
        if (grades.every((grade) => grade >= 0 && grade <= 100)) {
          this._grades = grades
        } else {
          this._grades = undefined
        }
      } else {
        this._grades = undefined
      }
    } else {
      this._grades = undefined
    }
  }

  login() {
    console.log(`${this._email} has logged in.`)
    return this
  }

  logout() {
    console.log(`${this._email} has logged out.`)
    return this
  }

  listGrades() {
    console.log(`${this._name}'s quarterly averages are: ${this._grades}`)
    return this
  }

  // compute for their grade average
  computeAverage() {
    this._avg =
      this._grades.reduce((acc, cur) => acc + cur, 0) / this._grades.length
    // console.log(this._avg)
    return this
  }

  printAvg() {
    console.log(`average: ${this._avg}`)
    return this
  }

  // determine if student avg is >= 85
  willPass() {
    this._passed = this.computeAverage() >= 85 ? true : false
    console.log(`passed: ${this._passed}`)
    return this
  }

  // >=90 with true, >=85 & <90 false, <85 undefinded
  //activity - make it chaneable
  willPassWithHonors() {
    this.computeAverage()
    const avg = this._avg
    switch (true) {
      case avg >= 90:
        this._withHonors = true
        break

      case avg >= 85 && avg < 90:
        this._withHonors = false
        break

      case avg < 85:
        this._withHonors = undefined
        break

      default:
        this._withHonors = 'error - average out of bound'
        break
    }
    console.log(`with honors: ${this._withHonors}`)
    return this
  }
}

const student = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85])
student.login()
console.log(student)
student.listGrades()
console.log(student.computeAverage())
console.log(student._avg)
console.log(student.willPass())
console.log(student.willPassWithHonors())
student.logout()
